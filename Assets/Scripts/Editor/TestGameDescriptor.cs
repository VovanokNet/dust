﻿using System.Collections.Generic;
using UnityEditor;

public class TestGameDescriptor
{
	[MenuItem("Dust/Create and save test game")]
	public static void CreateAndSaveTestGameDescriptor()
	{
		var testGameDescriptor = GetTestGameDescriptor();
		DescriptorWorker.SaveGameDescriptor(testGameDescriptor);
	}

	private static GameDescriptor GetTestGameDescriptor()
	{
		return new GameDescriptor(
			playerGameItemId: "Player",
			healthGameItemId: "Health",
			exitGameItemId: "Exit",
			gameItems: new List<GameItemDescriptor>
			{
				new CharacterDescriptor(
					id: "Player",
					damage: 1,
					health: 7,
					prefabName: "Characters/Hero"),
				new CharacterDescriptor(
					id: "Foe1",
					damage: 1,
					health: 2,
					prefabName: "Characters/Foe1"),
				new CharacterDescriptor(
					id: "Foe2",
					damage: 2,
					health: 4,
					prefabName: "Characters/Foe2"),
				new GameItemDescriptor(
					id: "Obstacle0",
					prefabName: "GameItems/Obstacle0"),
				new GameItemDescriptor(
					id: "Obstacle1",
					prefabName: "GameItems/Obstacle1"),
				new GameItemDescriptor(
					id: "Obstacle2",
					prefabName: "GameItems/Obstacle2"),
				new GameItemDescriptor(
					id: "Obstacle3",
					prefabName: "GameItems/Obstacle3"),
				new GameItemDescriptor(
					id: "Obstacle4",
					prefabName: "GameItems/Obstacle4"),
				new GameItemDescriptor(
					id: "Obstacle5",
					prefabName: "GameItems/Obstacle5"),
				new GameItemDescriptor(
					id: "ObstacleEmpty",
					prefabName: "GameItems/ObstacleEmpty"),
				new HealthDescriptor(
					id: "Health",
					prefabName: "GameItems/Heart",
					healthIncrementValue: 1),
				new GameItemDescriptor(
					id: "Exit",
					prefabName: "GameItems/Exit")
			},
			foesGameItemIds: new List<string>
			{
				"Foe1",
				"Foe2"
			},
			obstaclesGameItemIds: new List<string>
			{
				"Obstacle0",
				"Obstacle1",
				"Obstacle2",
				"Obstacle3",
				"Obstacle4",
				"Obstacle5"
			},
			levels: new List<LevelDescriptor>
			{
				new LevelDescriptor(
					dimX: 9,
					dimY: 9,
					cells: new List<string>
					{
						"ObstacleEmpty", string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, "ObstacleEmpty",
						string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
						string.Empty, string.Empty, string.Empty, "Foe1", string.Empty, "Obstacle3", "Obstacle4", "Obstacle4", string.Empty,
						string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, "Obstacle0", "Obstacle1", "Obstacle2", string.Empty,
						string.Empty, string.Empty, string.Empty, string.Empty, "Player", string.Empty, string.Empty, string.Empty, string.Empty,
						string.Empty, string.Empty, "Health", string.Empty, string.Empty, string.Empty, "Exit", string.Empty, string.Empty,
						string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
						string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, "Foe2", string.Empty, string.Empty,
						"ObstacleEmpty", string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, "ObstacleEmpty"
					})
			});
	}
}
