﻿using System;
using UnityEngine;

public class UserInputProvider : MonoBehaviour
{
	[SerializeField] private float minDistanceForSlidePercent;

	private Vector3 lastMousePosition;
	private float minDistanceForSlide;

	public event Action<InputEventEventArgs> OnInputEvent;

	private void Start()
	{
		minDistanceForSlide = Mathf.Min(Screen.width, Screen.height) * minDistanceForSlidePercent;
	}

	private void OnMouseUp()
	{
		DetectAction(Input.mousePosition - lastMousePosition);
	}

	private void OnMouseDown()
	{
		lastMousePosition = Input.mousePosition;
	}

	private void DetectAction(Vector2 deltaMousePosition)
	{
		if (deltaMousePosition.magnitude <= minDistanceForSlide)
		{
			return;
		}

		Side side;
		if (Mathf.Abs(deltaMousePosition.x) >= Mathf.Abs(deltaMousePosition.y))
		{
			//Horizontal slide
			side = deltaMousePosition.x < 0 ? Side.Left : Side.Right;
		}
		else
		{
			//Vertical slide
			side = deltaMousePosition.y < 0 ? Side.Down : Side.Up;
		}

		InputEvent(side);
	}

	private void InputEvent(Side side)
	{
		if (OnInputEvent != null)
		{
			OnInputEvent(new InputEventEventArgs(side));
		}
	}

	#region EventArgs

	public class InputEventEventArgs : EventArgs
	{
		public Side Side { get; private set; }

		public InputEventEventArgs(Side side)
		{
			Side = side;
		}
	}

	#endregion
}