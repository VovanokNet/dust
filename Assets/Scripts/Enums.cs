﻿public enum Side
{
	Left,
	Right,
	Up,
	Down
}
public enum AnimationClipType
{
	Idle,
	Attack,
	Move,
	Hurt,
	Die
}