﻿using System;
using UnityEngine;

public class CharacterIdleStateBehaviour : StateMachineBehaviour
{
	public event Action<EventArgs> OnEnter;
	public event Action<EventArgs> OnExit;

	public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		if (OnEnter != null)
		{
			OnEnter(new EventArgs());
		}
	}

	public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		if (OnExit != null)
		{
			OnExit(new EventArgs());
		}
	}
}
