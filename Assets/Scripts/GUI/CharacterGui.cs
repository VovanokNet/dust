﻿using UnityEngine;
using UnityEngine.UI;

public class CharacterGui : MonoBehaviour
{
	[SerializeField] private Text healthText;
	[SerializeField] private Text damageText;

	public void SetHealth(int health)
	{
		healthText.text = health > 0 ? health.ToString() : string.Empty;
	}

	public void SetDamage(int damage)
	{
		damageText.text = damage > 0 ? damage.ToString() : string.Empty;
	}

	public void Hide()
	{
		SetHealth(-1);
		SetDamage(-1);
	}
}