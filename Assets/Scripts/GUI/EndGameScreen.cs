﻿using System;
using UnityEngine;

public class EndGameScreen : MonoBehaviour
{
	[SerializeField] private GameObject winGui;
	[SerializeField] private GameObject failGui;

	public event Action<EventArgs> OnReplay;
	public event Action<EventArgs> OnShare;

	public void Show(bool isWin)
	{
		gameObject.SetActive(true);
		winGui.SetActive(isWin);
		failGui.SetActive(!isWin);
	}

	public void Hide()
	{
		gameObject.SetActive(false);
	}

	public void ReplayClick()
	{
		if (OnReplay != null)
		{
			OnReplay(new EventArgs());
		}
	}

	public void ShareClick()
	{
		if (OnShare != null)
		{
			OnShare(new EventArgs());
		}
	}
}
