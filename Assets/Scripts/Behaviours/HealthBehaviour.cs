﻿public class HealthBehaviour : GameItemBehaviour
{
	private HealthDescriptor healthDescriptor;
	public HealthDescriptor Descriptor {
		get
		{
			if (healthDescriptor == null)
			{
				healthDescriptor = descriptor as HealthDescriptor;
			}

			return healthDescriptor;
		}
	}

	public void Collect()
	{
		Delete();
	}
}
