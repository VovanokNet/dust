﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CharacterBehaviour : GameItemBehaviour
{
	[SerializeField] private float moveSpeedPercent = 2f;
	[SerializeField] private Transform guiPivot;
	[SerializeField] private GameObject guiPrefab;

	private static readonly Dictionary<AnimationClipType, string> clipNamePostfixes = new Dictionary<AnimationClipType, string>
		{
			{ AnimationClipType.Idle, "idle" },
			{ AnimationClipType.Attack, "attack" },
			{ AnimationClipType.Move, "move" },
			{ AnimationClipType.Hurt, "hurt" },
			{ AnimationClipType.Die,  "die" }
		};

	protected CharacterIdleStateBehaviour idleState;
	public event Action<EventArgs> OnEndTurn;
	
	private string ISMOVE_PARAM = "isMove";
	private string ISATTACK_PARAM = "isAttack";
	private string ISDEFEND_PARAM = "isDefend";
	private string ISDIE_PARAM = "isDie";

	private int health;
	protected int Health
	{
		get { return health; }
		set
		{
			health = value;
			gui.SetHealth(health);
		}
	}

	private bool isMove;
	private Vector2 startMovePoint;
	private Vector2 targetMovePoint;
	private float movePercent;
	private CharacterGui gui;
	protected bool isInIdleState;

	private CharacterDescriptor characterDescriptor;
	private CharacterDescriptor CharacterDescriptor
	{
		get
		{
			if (characterDescriptor == null)
			{
				characterDescriptor = descriptor as CharacterDescriptor;
			}

			return characterDescriptor;
		}
	}

	public override void Init(GameEnvironment gameEnvironment, GameItemDescriptor descriptor)
	{
		base.Init(gameEnvironment, descriptor);

		var guiGo = Instantiate(guiPrefab);
		guiGo.transform.SetParent(guiPivot);
		guiGo.transform.localPosition = Vector3.zero;
		gui = guiGo.GetComponent<CharacterGui>();

		Health = CharacterDescriptor.Health;
		gui.SetDamage(CharacterDescriptor.Damage);

		idleState = Animator.GetBehaviour<CharacterIdleStateBehaviour>();
		idleState.OnEnter += IdleEnter;
		idleState.OnExit += IdleExit;
	}

	public virtual void Turn()
	{
	}

	protected void StartMoveTo(Vector2 targetCoordOnGrid)
	{
		var selfCoordOnGrid = GetSelfCoordOnGrid();

		Animator.SetBool(ISMOVE_PARAM, true);

		SpriteRenderer.flipX = selfCoordOnGrid.x > targetCoordOnGrid.x;

		gameEnvironment.Grid.MoveItem(selfCoordOnGrid, targetCoordOnGrid);

		isMove = true;
		movePercent = 0;

		startMovePoint = transform.localPosition;
		targetMovePoint = gameEnvironment.Grid.GetRealCoord(targetCoordOnGrid);
	}

	protected Vector2 GetSelfCoordOnGrid()
	{
		return gameEnvironment.Grid.GetItemCoords(this);
	}

	protected void StartAttackCharacter(CharacterBehaviour targetCharacter, Side targetSide)
	{
		if (!targetCharacter.isInIdleState)
		{
			return;
		}

		Animator.SetBool(ISATTACK_PARAM, true);
		targetCharacter.Defend(CharacterDescriptor.Damage, targetSide);

		idleState.OnEnter += IdleEnterAfterAttack;
		Invoke("ToIdleAnimation", GetAnimationClip(AnimationClipType.Attack).length);
	}

	protected Vector2 GetNeiboorCoord(Vector2 selfCoord, Side side)
	{
		Vector2 neiboorCoord = selfCoord;
		switch (side)
		{
			case Side.Left:
				neiboorCoord.x--;
				break;
			case Side.Right:
				neiboorCoord.x++;
				break;
			case Side.Up:
				neiboorCoord.y--;
				break;
			case Side.Down:
				neiboorCoord.y++;
				break;
		}

		return neiboorCoord;
	}

	public virtual void EndTurn()
	{
		if (OnEndTurn != null)
		{
			OnEndTurn(new EventArgs());
		}
	}

	protected virtual void Update()
	{
		if (isMove)
		{
			movePercent += moveSpeedPercent * Time.deltaTime;
			transform.localPosition = Vector2.Lerp(startMovePoint, targetMovePoint, movePercent);

			if (movePercent >= 1)
			{
				StopMoveTo();
			}
		}
	}

	private void IdleEnterAfterAttack(EventArgs args)
	{
		idleState.OnEnter -= IdleEnterAfterAttack;
		EndTurn();
	}

	private void IdleEnter(EventArgs args)
	{
		isInIdleState = true;
	}

	private void IdleExit(EventArgs args)
	{
		isInIdleState = false;
	}

	private void StopMoveTo()
	{
		isMove = false;
		ToIdleAnimation();

		EndTurn();
	}

	private void ToIdleAnimation()
	{
		Animator.SetBool(ISATTACK_PARAM, false);
		Animator.SetBool(ISMOVE_PARAM, false);
		Animator.SetBool(ISATTACK_PARAM, false);
		Animator.SetBool(ISDEFEND_PARAM, false);
		Animator.SetBool(ISDIE_PARAM, false);
	}

	private void Defend(int damage, Side mySide)
	{
		Animator.SetBool(ISDEFEND_PARAM, true);

		if (mySide == Side.Right)
		{
			SpriteRenderer.flipX = true;
		}
		else if (mySide == Side.Left)
		{
			SpriteRenderer.flipX = false;
		}

		Health -= damage;
		if (Health > 0)
		{
			Invoke("ToIdleAnimation", GetAnimationClip(AnimationClipType.Hurt).length);
		}
		else
		{
			Die();
		}
	}

	protected virtual void Die()
	{
		Animator.SetBool(ISDIE_PARAM, true);
		gameEnvironment.Grid.SetItem(GetSelfCoordOnGrid(), null);
		gui.Hide();
	}

	protected AnimationClip GetAnimationClip(AnimationClipType type)
	{
		return Animator.runtimeAnimatorController.animationClips.FirstOrDefault(clip => clip.name.EndsWith(clipNamePostfixes[type]));
	}

	private void OnDestroy()
	{
		idleState.OnEnter -= IdleEnter;
		idleState.OnExit -= IdleExit;
	}
}
