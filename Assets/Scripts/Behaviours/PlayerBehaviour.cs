﻿using System;
using UnityEngine;

public class PlayerBehaviour : CharacterBehaviour
{
	public event Action<EventArgs> OnDie;
	public event Action<EventArgs> OnExit;

	private bool isLocked;
	ExitBehaviour exitBenaviour;

	public override void Turn()
	{
		base.Turn();
		isLocked = false;
	}

	public void TryMove(Side side)
	{
		if (!isInIdleState || isLocked)
		{
			return;
		}

		var selfCoordOnGrid = GetSelfCoordOnGrid();

		if ((side == Side.Left && selfCoordOnGrid.x <= 0) ||
			(side == Side.Right && selfCoordOnGrid.x >= gameEnvironment.Grid.DimX - 1) ||
			(side == Side.Up && selfCoordOnGrid.y <= 0) ||
			(side == Side.Down && selfCoordOnGrid.y >= gameEnvironment.Grid.DimY - 1))
		{
			return;
		}

		Vector2 targetCoordOnGrid = GetNeiboorCoord(selfCoordOnGrid, side);
		
		var gameItemOnWay = gameEnvironment.Grid.GetItem(targetCoordOnGrid);
		if (gameItemOnWay == null)
		{
			StartMoveTo(targetCoordOnGrid);
			return;
		}

		if (gameItemOnWay is CharacterBehaviour)
		{
			StartAttackCharacter((CharacterBehaviour)gameItemOnWay, side);
			return;
		}

		if (gameItemOnWay is HealthBehaviour)
		{
			TakeHealth((HealthBehaviour)gameItemOnWay);
			StartMoveTo(targetCoordOnGrid);
			return;
		}

		if (gameItemOnWay is ExitBehaviour)
		{
			exitBenaviour = (ExitBehaviour) gameItemOnWay;
			idleState.OnEnter += ExitAfterMoving;
			StartMoveTo(targetCoordOnGrid);
		}
	}

	private void ExitAfterMoving(EventArgs args)
	{
		idleState.OnEnter -= ExitAfterMoving;

		exitBenaviour.Enter();

		if (OnExit != null)
		{
			OnExit(new EventArgs());
		}
	}

	public override void EndTurn()
	{
		base.EndTurn();
		isLocked = true;
	}

	private void TakeHealth(HealthBehaviour healthBenaviour)
	{
		Health += healthBenaviour.Descriptor.HealthIncrementValue;
		healthBenaviour.Collect();
	}

	protected override void Die()
	{
		base.Die();

		Invoke("OnDieAlert", GetAnimationClip(AnimationClipType.Die).length + 2);
	}

	private void OnDieAlert()
	{
		if (OnDie != null)
		{
			OnDie(new EventArgs());
		}
	}
}
