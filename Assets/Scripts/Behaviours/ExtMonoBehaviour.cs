﻿using UnityEngine;

public class ExtMonoBehaviour : MonoBehaviour
{
	private Animator animator;

	public Animator Animator
	{
		get
		{
			if (animator == null)
			{
				animator = GetComponent<Animator>();

				if (animator == null)
				{
					animator = GetComponentInChildren<Animator>();
				}
			}

			return animator;
		}
	}

	private SpriteRenderer spriteRenderer;

	public SpriteRenderer SpriteRenderer
	{
		get
		{
			if (spriteRenderer == null)
			{
				spriteRenderer = GetComponent<SpriteRenderer>();

				if (spriteRenderer == null)
				{
					spriteRenderer = GetComponentInChildren<SpriteRenderer>();
				}
			}

			return spriteRenderer;
		}
	}
}
