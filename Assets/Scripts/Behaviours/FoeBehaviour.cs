﻿using System.Collections.Generic;
using UnityEngine;

public class FoeBehaviour : CharacterBehaviour
{
	private bool isTurned;

	public override void Turn()
	{
		base.Turn();
		if (Health <= 0)
		{
			EndTurn();
			return;
		}

		isTurned = true;
	}

	protected override void Update()
	{
		base.Update();

		if (isTurned && isInIdleState)
		{
			isTurned = false;
			MoveDecision();
		}
	}

	private void MoveDecision()
	{
		var selfCoord = GetSelfCoordOnGrid();

		//Решение об атаке
		Side attackTargetSide;
		if (IsNeighbor(selfCoord, out attackTargetSide))
		{
			StartAttackCharacter(gameEnvironment.Player, attackTargetSide);
			return;
		}

		var playerCoord = gameEnvironment.Grid.GetItemCoords(gameEnvironment.Player);
		var diffCoord = new Vector2(selfCoord.x - playerCoord.x, selfCoord.y - playerCoord.y);

		Stack<Side> movingPriority = new Stack<Side>(2);

		if (Mathf.Abs(diffCoord.x) < Mathf.Abs(diffCoord.y)) //В последнюю очередь по оси X
		{
			if (diffCoord.x < 0)
			{
				movingPriority.Push(Side.Right);
			}
			else if (diffCoord.x > 0)
			{
				movingPriority.Push(Side.Left);
			}

			if (diffCoord.y < 0)
			{
				movingPriority.Push(Side.Down);
			}
			else if (diffCoord.y > 0)
			{
				movingPriority.Push(Side.Up);
			}
		}
		else //Движение по оси Y
		{
			if (diffCoord.y < 0)
			{
				movingPriority.Push(Side.Down);
			}
			else if (diffCoord.y > 0)
			{
				movingPriority.Push(Side.Up);
			}

			if (diffCoord.x < 0)
			{
				movingPriority.Push(Side.Right);
			}
			else if (diffCoord.x > 0)
			{
				movingPriority.Push(Side.Left);
			}
		}

		Side tryingSide;
		Vector2 neiboorCoord;

		while (movingPriority.Count > 0)
		{
			tryingSide = movingPriority.Pop();
			neiboorCoord = GetNeiboorCoord(selfCoord, tryingSide);

			if (0 <= neiboorCoord.x && neiboorCoord.x < gameEnvironment.Grid.DimX &&
				0 <= neiboorCoord.y && neiboorCoord.y < gameEnvironment.Grid.DimY &&
				gameEnvironment.Grid.GetItem(neiboorCoord) == null)
			{
				StartMoveTo(neiboorCoord);
				return;
			}
		}

		EndTurn();
	}

	private bool IsNeighbor(Vector2 selfCoord, out Side neighborSide)
	{
		if (selfCoord.x + 1 < gameEnvironment.Grid.DimX &&
			gameEnvironment.Grid.GetItem(new Vector2(selfCoord.x + 1, selfCoord.y)) == gameEnvironment.Player)
		{
			neighborSide = Side.Right;
			return true;
		}

		if (selfCoord.x - 1 >= 0 &&
			gameEnvironment.Grid.GetItem(new Vector2(selfCoord.x - 1, selfCoord.y)) == gameEnvironment.Player)
		{
			neighborSide = Side.Left;
			return true;
		}

		if (selfCoord.y + 1 < gameEnvironment.Grid.DimY &&
			gameEnvironment.Grid.GetItem(new Vector2(selfCoord.x, selfCoord.y + 1)) == gameEnvironment.Player)
		{
			neighborSide = Side.Down;
			return true;
		}

		if (selfCoord.y - 1 >= 0 &&
			gameEnvironment.Grid.GetItem(new Vector2(selfCoord.x, selfCoord.y - 1)) == gameEnvironment.Player)
		{
			neighborSide = Side.Up;
			return true;
		}

		neighborSide = Side.Left;
		return false;
	}
}
