﻿public class GameItemBehaviour : ExtMonoBehaviour
{
	protected GameEnvironment gameEnvironment;
	protected GameItemDescriptor descriptor;

	public virtual void Init(GameEnvironment gameEnvironment, GameItemDescriptor descriptor)
	{
		this.gameEnvironment = gameEnvironment;
		this.descriptor = descriptor;
	}

	protected void Delete()
	{
		gameEnvironment.Grid.SetItem(gameEnvironment.Grid.GetItemCoords(this), null);
		gameObject.SetActive(false);
	}
}