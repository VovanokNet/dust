﻿using UnityEngine;

public class GameItemsGrid
{
	public int DimX
	{
		get { return items.GetLength(0); }
	}

	public int DimY
	{
		get { return items.GetLength(1); }
	}

	private GameItemBehaviour[,] items;
	private Vector2 cellSize;

	public GameItemsGrid(Vector2 dim, Vector2 cellSize)
	{
		this.cellSize = cellSize;
		items = new GameItemBehaviour[(int)dim.x, (int)dim.y];
	}

	public void SetItem(Vector2 coords, GameItemBehaviour item)
	{
		items[(int) coords.x, (int) coords.y] = item;
	}

	public GameItemBehaviour GetItem(Vector2 coords)
	{
		return items[(int)coords.x, (int)coords.y];
	}

	public void MoveItem(Vector2 srcCoord, Vector2 dstCoord)
	{
		SetItem(dstCoord, GetItem(srcCoord));
		SetItem(srcCoord, null);
	}

	public Vector2 GetItemCoords(GameItemBehaviour gameItem)
	{
		Vector2 result;
		for (result.x = 0; result.x < DimX; result.x++)
		{
			for (result.y = 0; result.y < DimY; result.y++)
			{
				if (GetItem(result) == gameItem)
				{
					return result;
				}
			}
		}

		return Vector2.zero;
	}

	public Vector2 GetRealCoord(Vector2 gridCoords)
	{
		return new Vector2(
			(gridCoords.x + 0.5f * (1 - DimX)) * cellSize.x,
			-(gridCoords.y + 0.5f * (1 - DimY)) * cellSize.y);
	}
}
