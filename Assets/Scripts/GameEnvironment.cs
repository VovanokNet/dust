﻿using System;
using UnityEngine;
using System.Linq;
using System.Collections.Generic;

public class GameEnvironment : MonoBehaviour
{
	[SerializeField] private GameObject floorTilePrefab;
	[SerializeField] private int floorTilesOverlayX;
	[SerializeField] private int floorTilesOverlayY;
	[SerializeField] private Transform floorTilesContainer;
	[SerializeField] private UserInputProvider userInputProvider;
	[SerializeField] private EndGameScreen endGameScreen;
	[SerializeField] private Transform charactersContainer;
	[SerializeField] private Transform gameItemsContainer;

	public GameItemsGrid Grid { get; private set; }
	public PlayerBehaviour Player { get; private set; }

	private GameDescriptor gameDescriptor;
	private LevelDescriptor curLevel;
	private int curCharacterNum;
	private List<GameItemBehaviour> allGameItems = new List<GameItemBehaviour>();
	private List<CharacterBehaviour> characters = new List<CharacterBehaviour>();
	private List<GameObject> floorTiles = new List<GameObject>();

	private void Start()
	{
		gameDescriptor = DescriptorWorker.LoadGameDescriptor();
		userInputProvider.OnInputEvent += InputProviderEvent;

		endGameScreen.OnShare += EndGameGuiShareClick;
		endGameScreen.OnReplay += EndGameGuiReplayClick;

		LoadLevel(0);
	}

	private void TurnNextCharacter()
	{
		characters[curCharacterNum].OnEndTurn += EndCharacterTurn;
		characters[curCharacterNum].Turn();
	}

	private void EndCharacterTurn(EventArgs args)
	{
		characters[curCharacterNum].OnEndTurn -= EndCharacterTurn;

		curCharacterNum++;
		curCharacterNum %= characters.Count;

		TurnNextCharacter();
	}

	private void InputProviderEvent(UserInputProvider.InputEventEventArgs args)
	{
		Player.TryMove(args.Side);
	}

	private GameObject CreateGridEntity(GameObject entityPrefab, Vector2 gridCoord, Transform container)
	{
		var entity = Instantiate(entityPrefab);
		entity.transform.SetParent(container);
		entity.transform.localPosition = Grid.GetRealCoord(gridCoord);
		return entity;
	}

	private void InitFoor()
	{
		foreach (var floorTile in floorTiles)
		{
			Destroy(floorTile);
		}

		floorTiles.Clear();

		//Инстанцируем тайлы пола, смещение нужно для того, чтоыб замостить не толко сетку 9х9, но и края
		Vector2 gridCoords;
		for (gridCoords.x = -floorTilesOverlayX; gridCoords.x < curLevel.DimX + floorTilesOverlayX; gridCoords.x++)
		{
			for (gridCoords.y = -floorTilesOverlayY; gridCoords.y < curLevel.DimY + floorTilesOverlayY; gridCoords.y++)
			{
				floorTiles.Add(CreateGridEntity(floorTilePrefab, gridCoords, floorTilesContainer));
			}
		}
	}

	private void LoadLevel(int levelNum)
	{
		endGameScreen.Hide();

		curLevel = gameDescriptor.Levels[levelNum];
		
		if (curLevel == null || curLevel.Cells == null)
		{
			Debug.LogError("Error load null level");
			return;
		}

		DestroyGameItems();
		Grid = new GameItemsGrid(new Vector2(curLevel.DimX, curLevel.DimY), floorTilePrefab.GetComponent<SpriteRenderer>().bounds.size);

		InitFoor();
		characters.Clear();

		Vector2 gridCoord;
		string curGameItemId;
		GameItemDescriptor curGameItemDescriptor;
		GameObject loadedGameItem;
		GameObject newGameItemGo;
		GameItemBehaviour newGameItem;
		bool isCharacter;

		//Инстанцирование игровых объектов по описанию уровня
		for (int i = 0; i < curLevel.Cells.Count; i++)
		{
			gridCoord.x = i % curLevel.DimX;
			gridCoord.y = i / curLevel.DimX;

			if (gridCoord.y >= curLevel.DimY)
			{
				break;
			}

			curGameItemId = curLevel.Cells[i];
			if (string.IsNullOrEmpty(curGameItemId))
			{
				continue;
			}

			//По названию встретившегося в сетке гейм-итема находим его описание в справочнике
			curGameItemDescriptor = gameDescriptor.GameItems.FirstOrDefault(item => item.Id == curGameItemId);
			if (curGameItemDescriptor == null)
			{
				Debug.LogErrorFormat("Unknown game item in level ({0} - {1})", curGameItemId, gridCoord);
				continue;
			}

			loadedGameItem = Resources.Load<GameObject>(curGameItemDescriptor.PrefabName);
			if (loadedGameItem == null)
			{
				Debug.LogErrorFormat("Load resource {0} error in cell ({1})", curGameItemDescriptor.PrefabName, gridCoord);
				continue;
			}

			isCharacter = curGameItemId == gameDescriptor.PlayerGameItemId ||
			              gameDescriptor.FoesGameItemIds.Any(item => item == curGameItemId);

			newGameItemGo = CreateGridEntity(loadedGameItem, gridCoord, isCharacter ? charactersContainer : gameItemsContainer);
			newGameItem = newGameItemGo.GetComponent<GameItemBehaviour>();
			newGameItem.Init(this, curGameItemDescriptor);
			allGameItems.Add(newGameItem);

			//Добавление в список персонажей (игрок на первом месте)
			if (curGameItemId == gameDescriptor.PlayerGameItemId)
			{
				Player = newGameItem as PlayerBehaviour;

				if (Player == null)
				{
					Debug.LogError("Player has incorrect behaviour");
					return;
				}

				Player.OnDie += PlayerDie;
				Player.OnExit += PlayerExit;
				characters.Insert(0, Player);
			}
			else if (isCharacter)
			{
				characters.Add(newGameItem as CharacterBehaviour);
			}

			Grid.SetItem(gridCoord, newGameItem);
		}

		StartGameWithPlayer();
	}

	private void StartGameWithPlayer()
	{
		curCharacterNum = 0;
		TurnNextCharacter();
	}

	private void PlayerDie(EventArgs args)
	{
		Player.OnDie -= PlayerDie;
		endGameScreen.Show(false);
	}

	private void PlayerExit(EventArgs args)
	{
		Player.OnExit -= PlayerExit;
		endGameScreen.Show(true);
	}

	private void EndGameGuiReplayClick(EventArgs args)
	{
		LoadLevel(0);
	}

	private void EndGameGuiShareClick(EventArgs args)
	{
	}

	private void DestroyGameItems()
	{
		if (Player != null)
		{
			Player.OnDie -= PlayerDie;
			Player.OnExit -= PlayerExit;
		}

		foreach (var gameItem in allGameItems)
		{
			Destroy(gameItem.gameObject);
		}

		allGameItems.Clear();
	}
}
