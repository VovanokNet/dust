﻿using System;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

public static class DescriptorWorker
{
	private const string DESCRIPTOR_FILENAME = "GameDescriptor.xml";
	private static readonly XmlSerializer serializer = new XmlSerializer(typeof(GameDescriptor));

	public static void SaveGameDescriptor(GameDescriptor gameDescriptor)
	{
		try
		{
			var gameDescriptorFileName = Path.Combine(Path.Combine(Application.dataPath, "Resources"), DESCRIPTOR_FILENAME);
			using (TextWriter writer = new StreamWriter(gameDescriptorFileName))
			{
				serializer.Serialize(writer, gameDescriptor);
			}
		}
		catch (Exception ex)
		{
			Debug.LogError("Save game descriptor error: " + ex.Message);
			throw;
		}
	}

	public static GameDescriptor LoadGameDescriptor()
	{
		try
		{
			var serializedGameDescriptor = Resources.Load<TextAsset>(Path.GetFileNameWithoutExtension(DESCRIPTOR_FILENAME));

			using (TextReader reader = new StringReader(serializedGameDescriptor.text))
			{
				return (GameDescriptor)serializer.Deserialize(reader);
			}
		}
		catch (Exception ex)
		{
			Debug.LogError("Load game descriptor error: " + ex.Message);
			return null;
		}
	}
}
