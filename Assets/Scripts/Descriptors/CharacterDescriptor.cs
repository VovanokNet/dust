﻿using System;
using System.Xml.Serialization;

[Serializable]
public class CharacterDescriptor : GameItemDescriptor
{
	[XmlAttribute("health")]
	public int Health { get; private set; }

	[XmlAttribute("damage")]
	public int Damage { get; private set; }

	public CharacterDescriptor() : this(string.Empty, string.Empty, 0, 0)
	{
	}

	public CharacterDescriptor(string id, string prefabName, int health, int damage)
		: base(id, prefabName)
	{
		Health = health;
		Damage = damage;
	}
}
