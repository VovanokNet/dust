﻿using System;
using System.Xml.Serialization;

[Serializable]
public class HealthDescriptor : GameItemDescriptor
{
	[XmlAttribute("healthIncrementValue")]
	public int HealthIncrementValue { get; private set; }

	public HealthDescriptor() : this(string.Empty, string.Empty, 0)
	{
	}

	public HealthDescriptor(string id, string prefabName, int healthIncrementValue)
		: base(id, prefabName)
	{
		HealthIncrementValue = healthIncrementValue;	
	}
}
