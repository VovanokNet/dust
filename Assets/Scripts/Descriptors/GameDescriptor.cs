﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("Game")]
public class GameDescriptor
{
	[XmlAttribute("playerGameItemId")]
	public string PlayerGameItemId { get; private set; }

	[XmlAttribute("healthGameItemId")]
	public string HealthGameItemId { get; private set; }

	[XmlAttribute("exitGameItemId")]
	public string ExitGameItemId { get; private set; }

	[XmlArray("GameItems")]
	[XmlArrayItem("GameItem")]
	public List<GameItemDescriptor> GameItems { get; private set; }

	[XmlArray("FoesGameItemIds")]
	[XmlArrayItem("FoeGameItemId")]
	public List<string> FoesGameItemIds { get; private set; }

	[XmlArray("ObstaclesGameItemIds")]
	[XmlArrayItem("ObstacleGameItemId")]
	public List<string> ObstaclesGameItemIds { get; private set; }

	[XmlArray("Levels")]
	[XmlArrayItem("Level")]
	public List<LevelDescriptor> Levels { get; private set; }

	public GameDescriptor() : this(string.Empty, string.Empty, string.Empty, new List<GameItemDescriptor>(),
		new List<string>(), new List<string>(), new List<LevelDescriptor>())
	{
	}

	public GameDescriptor(string playerGameItemId, string healthGameItemId, string exitGameItemId,
		List<GameItemDescriptor> gameItems, List<string> foesGameItemIds, List<string> obstaclesGameItemIds, List<LevelDescriptor> levels)
	{
		PlayerGameItemId = playerGameItemId;
		HealthGameItemId = healthGameItemId;
		ExitGameItemId = exitGameItemId;
		GameItems = gameItems;
		FoesGameItemIds = foesGameItemIds;
		ObstaclesGameItemIds = obstaclesGameItemIds;
		Levels = levels;
	}

	public T GetGameItem<T>(string id) where T : GameItemDescriptor
	{
		return GameItems.FirstOrDefault(item => item.Id == id) as T;
	}
}
