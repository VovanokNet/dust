﻿using System;
using System.Xml.Serialization;

[Serializable]
[XmlInclude(typeof(CharacterDescriptor))]
[XmlInclude(typeof(HealthDescriptor))]
public class GameItemDescriptor
{
	[XmlAttribute("id")]
	public string Id { get; private set; }

	[XmlAttribute("prefabName")]
	public string PrefabName { get; private set; }

	public GameItemDescriptor() : this(string.Empty, string.Empty)
	{
	}

	public GameItemDescriptor(string id, string prefabName)
	{
		Id = id;
		PrefabName = prefabName;
	}
}
