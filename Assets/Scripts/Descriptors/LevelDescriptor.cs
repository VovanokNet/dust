﻿using System.Collections.Generic;
using System.Xml.Serialization;

public class LevelDescriptor
{
	[XmlAttribute("dimX")]
	public int DimX { get; private set; }

	[XmlAttribute("dimY")]
	public int DimY { get; private set; }

	[XmlArray("Cells")]
	[XmlArrayItem("Cell")]
	public List<string> Cells { get; private set; }

	public LevelDescriptor() : this(0, 0, new List<string>())
	{
	}

	public LevelDescriptor(int dimX, int dimY, List<string> cells)
	{
		DimX = dimX;
		DimY = dimY;
		Cells = cells;
	}
}
